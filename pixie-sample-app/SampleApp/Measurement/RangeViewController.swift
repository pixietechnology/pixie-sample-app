import UIKit
import CoreBluetooth
import PXPixieSDK_New

class RangeViewController: BaseViewController,PXPixieManagerDelegate, PixiePointDelegate {
    
    @IBOutlet weak var txtRanges: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    var fromPoint:PXPixiePoint!
    var toPoints:[PXPixiePoint]!
    
    override func viewDidLoad() {
        self.txtRanges.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.titleLabel.text = "showing ranges..."
        self.requestRanges()
    }
    
    @IBAction func didPressBack(_ sender: Any) {
        sdk.stopRanges(completion: nil)
        self.dismiss(animated: true, completion:nil)
    }
    
    //MARK: -
    //MARK: Private Methods
    
    private func generateAllPointsMatrix(firstPoint:PXPixiePoint, otherPoints: [PXPixiePoint]) -> [(PXPixiePoint, PXPixiePoint)]{
        
        var allPoints: [PXPixiePoint] = otherPoints
        allPoints.append(firstPoint)
        return self.generateMatrix(allPoints: allPoints)
    }
    
    private func generateMatrix<T>(allPoints: [T]) -> [(T, T)]{
        
        var tupplePoints:[(T, T)] = []
        
        var startPoints = allPoints
        var otherPoints = allPoints
        
        while startPoints.count > 0{
            otherPoints.removeFirst()
            for p in otherPoints{
                tupplePoints.append((startPoints.first!,p))
            }
            startPoints.removeFirst()
        }
        
        return tupplePoints
    }
    
    func requestRanges(){
        
        var rangeRequestTuppleArray:[(PXPixiePoint, PXPixiePoint)] = []
        
        /*
         ------------
         DEVELOPER ENTRY POINT 2
         
         This area will generate a tuple array of range requests from point to point: example [(a,b), (a,c), (a,d)]
         The sample app requests ranging from one point to the rest of the points
         
         You can request the full range matrix (from all points to all points) by changing: rangeAllToAll = true
         
         You can also make your own rangeRequestTuppleArray array
         ------------
         */
        
        let rangeAllToAll: Bool = false
        if (rangeAllToAll) {
            rangeRequestTuppleArray = self.generateAllPointsMatrix(firstPoint: fromPoint, otherPoints: toPoints)
        } else{
          toPoints.forEach({ (toPixiePoint) in
              rangeRequestTuppleArray.append((fromPoint,toPixiePoint))
          })
        }
        
        sdk.delegate = self
        for point in self.toPoints {
            sdk.setPointDelegate(point: point, delegate: self)
        }
      
        sdk.startRanges(pairs: rangeRequestTuppleArray, rangeCount: 0, frequency: .Slow, completion: { error in
        if let _error = error {
          print(_error.simpleDescription())
        }
      })
    }
    
    //MARK: -
    //MARK: PXPixieManagerDelegate
    
    /**
     Receive events when Core Bluetooth state changes. See Apple documentation for CentralManagerState
     */
    func pixieManager(_ manager: PXPixieManager, didChangeStateFrom previousState: CentralManagerState, To currentState: CentralManagerState) {
        
    }
    
    func pointsAreRangeable(points: [PXPixiePoint]){}
    
    func didSetCoordinator(point: PXPixiePoint) {}
    
    func didScanPoint(point: PXPixiePoint){}
    
    func didGetRangesFromPoints(ranges: [PXRange]){
        DispatchQueue.main.async {
            var text:String = ""
            ranges.forEach({ (range) in
                /*
                 ------------
                 DEVELOPER ENTRY POINT 1
                 
                 Here you will actually get the ranges (range.range). 
                 make sure that the range status is good (range.rangeStatus == .Success)
                 ------------
                */
                
                let rangeText: String = range.rangeStatus == .Success ? "\(range.range)": "error"
                text += "\(range.pair.0.macAddressString) To \(range.pair.1.macAddressString): \(rangeText)\n"
            })
            self.txtRanges.text! = text
            let range = NSMakeRange(self.txtRanges.text.characters.count - 1, 0)
            self.txtRanges.scrollRangeToVisible(range)
        }
    }
    
    func pixiePointDidConnect(point: PXPixiePoint) {
        if(point == self.fromPoint || toPoints.contains(point)) {
            DispatchQueue.main.async {
                self.txtRanges.text! = "\(point.getPixieStringID()) did connect"
                let range = NSMakeRange(self.txtRanges.text.characters.count - 1, 0)
                self.txtRanges.scrollRangeToVisible(range)
            }
        }
    }
    
    func pixiePointDidDisconnect(point: PXPixiePoint) {
        if(point == self.fromPoint || toPoints.contains(point)) {
            DispatchQueue.main.async {
                self.txtRanges.text! = "\(point.getPixieStringID()) did disconnect"
                let range = NSMakeRange(self.txtRanges.text.characters.count - 1, 0)
                self.txtRanges.scrollRangeToVisible(range)
            }
        }
    }
    
    func point(point: PXPixiePoint, didUpdateBatteryStatus batteryStatus: Pixie.BatteryStatus) {}
    
    func point(point: PXPixiePoint, didReadBatLevel level: UInt8?, withStatus: PXReadBatteryResult) {}
    
    func didRefreshPoints() {}
}
