import UIKit

class ConnectToPointCollectionViewCell: PointCollectionViewCell {
    
    @IBOutlet weak var lblMacAddress: UILabel!
    @IBOutlet weak var btnCoordinator: UIButton!
    @IBOutlet weak var btnSelect: UIButton!
    
    var displayAsCoordinator: Bool = false
    var displayAsSelected: Bool = false
    
    override func draw() {
        
        guard let _point = self.point else{
            return
        }
        
        self.lblMacAddress.text = _point.macAddressString
        
        self.btnCoordinator.backgroundColor = self.displayAsCoordinator ? UIColor.blue : UIColor.gray
        self.btnSelect.backgroundColor = self.displayAsSelected ? UIColor.purple : UIColor.gray
        
        self.btnSelect.setTitle(self.displayAsSelected ? "Unselect" : "Select", for: .normal)
        self.btnSelect.isHidden = self.displayAsCoordinator
        self.backgroundColor = _point.pointColor.uiColor.withAlphaComponent(0.1)
    }
    
    //MARK: -
    // Cell Buttons
    @IBAction func didPressSelect(_ sender: Any) {
        
        if(!self.displayAsSelected) {
            self.cellDelegate?.connectPoint(sender: self)
        }
        else {
            self.cellDelegate?.disconnectPoint(sender: self)
        }
        
    }
    @IBAction func didPressCoordinator(_ sender: Any) {
        self.cellDelegate?.setCoordinator(sender: self)
    }

}
