import UIKit
import CoreBluetooth
import PXPixieSDK_New

protocol PointUICellDelegate: class{
    func connectPoint(sender: PointCollectionViewCell)
    func disconnectPoint(sender: PointCollectionViewCell)
    func showRanges(sender: PointCollectionViewCell)
    func setCoordinator(sender: PointCollectionViewCell)
}

class PointListViewController: BaseViewController, PointUICellDelegate, UICollectionViewDataSource,PXPixieManagerDelegate,PixiePointDelegate {
    
    static let fileName = "PointListViewController.swift"
    
    @IBOutlet weak var rtlsButton: UIButton!
    //MARK: Variables    
    @IBOutlet weak var pointCollection: UICollectionView!
    @IBOutlet weak var greetingsLabel: UILabel!
    
    @IBOutlet weak var reloadingLabel: UILabel!
    
    var currentCoordinator : String?
    
    
    //MARK: -
    //MARK: Storyboard actions
    
    @IBAction func retrieveTagsFromServer(_ sender: Any) {
        
        pxlog("user pressed retrieve")
        
        self.pointCollection.isHidden = true
        self.reloadingLabel.text = "Fetching from server..."
        self.reloadingLabel.isHidden = false
        
        sdk.restorePoints { [weak self] coordinator, error in
            
            if let _error = error{
                print(_error.simpleDescription())
                if(error == PXError.UserIsLoggedOut) {
                    DispatchQueue.main.async {
                        self?.showErrorAlert(message: _error.simpleDescription(), handler: { action in
                            self?.didPressLogoutButton(sender: self!)
                        })
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self?.showErrorAlert(message: _error.simpleDescription(), handler: nil)
                    }
                }
            }

            DispatchQueue.main.async {
                
                self?.reloadingLabel.isHidden = true
                self?.pointCollection.isHidden = false
                self?.currentCoordinator = sdk.coordinatorPoint?.macAddressString
                self?.pointCollection.reloadData()
            }
            
        }
    }
    
    func didRefreshPoints() {
        
        self.currentCoordinator = sdk.coordinatorPoint?.macAddressString
        
        DispatchQueue.main.async {
            self.pointCollection.reloadData()
        }
    }
    
    @IBAction func didPressLogoutButton(sender: AnyObject) {
      pxlog("user pressed logout")
      sdk.logout(completion: { result, error in
        guard error == nil else { print(error!.simpleDescription()); return }
        self.performSegue(withIdentifier: "goToAuthenticate", sender: self)
      })
    }
    
    @IBAction func didPressDisconnectAll(_ sender: Any) {
        pxlog("user pressed disconnect all")
        let macs : [String] = sdk.pairedPoints.map{$0.macAddressString}
        sdk.disconnectFromPoints(pointAddress: macs, completion: nil)
    }
    
    //MARK: -
    //MARK: Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        guard sdk.isAuthenticated else {
            self.performSegue(withIdentifier: "goToAuthenticate", sender: self)
            return
        }
        
        sdk.delegate = self
        self.pointCollection.dataSource = self
        
        self.reloadingLabel.isHidden = true
        self.pointCollection.isHidden = false
        
        self.pointCollection.reloadData()
        greetingsLabel.text = sdk.userEmail!
        
        self.currentCoordinator = sdk.coordinatorPoint?.macAddressString
        
        self.checkIfRtlsAvailable()
        
    }
    
    func checkIfRtlsAvailable(){
        let numberOfConnectedPoints = (sdk.pairedPoints.filter { $0.pointState == PXState.CONNECTED && $0.canRequestRanges == true }).count
        self.rtlsButton.isEnabled = (numberOfConnectedPoints >= 4)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToRanges"){
            
            let rangeViewController = segue.destination as! RangeViewController
            let args = sender as! [String:AnyObject]
            rangeViewController.fromPoint = args["FromPoint"] as! PXPixiePoint
            rangeViewController.toPoints = args["ToPoints"] as! [PXPixiePoint]
        }
        
    }
    
    //MARK: -
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return sdk.pairedPoints.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PointCell", for: indexPath as IndexPath) as! PointMainCollectionViewCell
        
        let point = sdk.pairedPoints[indexPath.row]
        sdk.setPointDelegate(point: point, delegate: self)
        
        cell.cellDelegate = self
        cell.point = point
        cell.displayAsCoordinator = self.currentCoordinator == cell.point?.macAddressString
        cell.displayAsRangeable = point.canRequestRanges == true && point.pointState == .CONNECTED
        cell.draw()
        
        return cell
    }
    
    //MARK: -
    //MARK: PointUIOperationDelegate
    
    func connectPoint(sender: PointCollectionViewCell){

        
    }
    
    func disconnectPoint(sender: PointCollectionViewCell){

    }
    
    func showRanges(sender: PointCollectionViewCell){
        
        pxlog("user clicked showRanges on \(sender.point!.macAddressString)", fileName: PointListViewController.fileName)
        
        guard let fromPoint = sender.point else { print("Point is not connected"); return }
        
        let otherPoints = sdk.pairedPoints.filter { (p) -> Bool in
                            return p.macAddressString != fromPoint.macAddressString
                        }
        self.performSegue(withIdentifier: "goToRanges", sender: ["FromPoint":fromPoint,"ToPoints":otherPoints])
    }
    
    func setCoordinator(sender: PointCollectionViewCell){
        


    }
    
    //MARK: -
    //MARK: PXPixieManagerDelegate
    
    /**
     Receive events when Core Bluetooth state changes. See Apple documentation for CentralManagerState
     */
    func pixieManager(_ manager: PXPixieManager, didChangeStateFrom previousState: CentralManagerState, To currentState: CentralManagerState) {
        
    }
    
    func pointsAreRangeable(points: [PXPixiePoint]){
        
        pxlog("rangeable points: \(points.count)", fileName: PointListViewController.fileName)

        DispatchQueue.main.async { [weak self] in
            self?.checkIfRtlsAvailable()
            self?.pointCollection.reloadData()
        }
    }
    
    func didScanPoint(point: PXPixiePoint){}
    
    func didSetCoordinator(point: PXPixiePoint) {
        
        pxlog("Did set coordinator \(point.macAddressString)", fileName: PointListViewController.fileName)
        
        DispatchQueue.main.async { [weak self] in
            self?.pointCollection.reloadData()
        }
    }
    
    func didGetRangesFromPoints(ranges: [PXRange]){}
    
    //MARK: -
    //MARK: PixiePointDelegate
    
    func pixiePointDidConnect(point pixiePoint: PXPixiePoint){
        pxlog("Did connect to \(pixiePoint.macAddressString)", fileName: PointListViewController.fileName)
        
        DispatchQueue.main.async { [weak self] in
            self?.checkIfRtlsAvailable()
            self?.pointCollection.reloadData()
        }
        
    }
    
    func pixiePointDidDisconnect(point pixiePoint: PXPixiePoint){
        pxlog("Did disconnect from \(pixiePoint.macAddressString)", fileName: PointListViewController.fileName)
        
        DispatchQueue.main.async { [weak self] in
            self?.checkIfRtlsAvailable()
            self?.pointCollection.reloadData()
        }
    }
    
    func point(point: PXPixiePoint, didUpdateBatteryStatus batteryStatus: Pixie.BatteryStatus){}
    
    func point(point: PXPixiePoint, didReadBatLevel level: UInt8?, withStatus: PXReadBatteryResult) {}
    
}

