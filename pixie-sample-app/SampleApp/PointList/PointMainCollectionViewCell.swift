import UIKit

class PointMainCollectionViewCell: PointCollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rangeableButton: UIButton!
    @IBOutlet weak var coordinatorIndicator: UILabel!

    var displayAsCoordinator : Bool = false
    var displayAsRangeable : Bool = false
    
    override func draw() {
        
        guard let _point = self.point else{
            return
        }
        
        self.rangeableButton.isHidden = !self.displayAsRangeable
        self.coordinatorIndicator.isHidden = !self.displayAsCoordinator
        
        self.nameLabel.text = _point.macAddressString
        let isConnected = _point.pointState == .CONNECTED
        self.backgroundColor = _point.pointColor.uiColor.withAlphaComponent(isConnected ? 1 : 0.1)
    }
    
    @IBAction func didPrerssRange(_ sender: Any) {
        self.cellDelegate?.showRanges(sender: self)
    }
    
}
