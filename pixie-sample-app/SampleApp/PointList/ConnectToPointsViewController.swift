import UIKit
import PXPixieSDK_New

class ConnectToPointsViewController: UIViewController, PointUICellDelegate, UICollectionViewDataSource {

    @IBOutlet weak var pointsCollectionView: UICollectionView!
    
    var displayAsConnectPoints:[String:Bool] = [:]
    var currentCoordinator : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewDidAppear(_ animated: Bool) {
        

        self.pointsCollectionView.dataSource = self
        self.pointsCollectionView.reloadData()
        
        self.pointsCollectionView.isHidden = false
        
        self.pointsCollectionView.reloadData()
        
        self.currentCoordinator = sdk.coordinatorPoint?.macAddressString
        
        super.viewDidAppear(animated)
        
        for point in sdk.pairedPoints {
            print("paired point detected: \(point.macAddressString)")
            self.displayAsConnectPoints[point.macAddressString] = (point.pointState == PXState.CONNECTED)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: -
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return sdk.pairedPoints.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PointCell", for: indexPath as IndexPath) as! ConnectToPointCollectionViewCell
        
        let point = sdk.pairedPoints[indexPath.row]
        
        cell.cellDelegate = self
        cell.point = point
        cell.displayAsCoordinator = self.currentCoordinator == cell.point?.macAddressString
        
        let isConnect = self.displayAsConnectPoints.contains(where: { k, v -> Bool in
            return k == point.macAddressString && v == true
        })
        cell.displayAsSelected = isConnect
        cell.draw()
        
        return cell
    }

    @IBAction func didPressConnect(_ sender: Any) {
        
        guard let coordinator = self.currentCoordinator else {
            showErrorAlert(message: "You must choose coordinator")
            return
        }
        let pointsToConnect = self.displayAsConnectPoints.filter({ (mac,connectable) -> Bool in
            connectable == true
        }).flatMap {$0.key}
        var pointsToDisconnect = self.displayAsConnectPoints.filter({ (mac,connectable) -> Bool in
            connectable == false
        }).flatMap {$0.key}
        if let index = pointsToDisconnect.index(of: coordinator) {
            pointsToDisconnect.remove(at: index)
        }
        guard pointsToConnect.count > 0 else {
            showErrorAlert(message: "You must choose at least one point to connect")
            return
        }
        
        sdk.disconnectFromPoints(pointAddress: pointsToDisconnect) { [weak self] (error) in
            if let _error = error {
                self?.showErrorAlert(message: _error.simpleDescription())
                return
            }
            else {
                sdk.connectToPoints(coordinatorPointAddress: coordinator, pointAddresses: pointsToConnect) { [weak self] (error) in
                    if let _error = error {
                        self?.showErrorAlert(message: _error.simpleDescription())
                        return
                    }
                    else {
                        self?.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    @IBAction func didPressBAck(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: -
    //MARK: PointUIOperationDelegate
    
    func connectPoint(sender: PointCollectionViewCell){
        
        let mac = sender.point!.macAddressString
        pxlog("user clicked select on \(mac)", fileName: PointListViewController.fileName)
        DispatchQueue.main.async { [weak self] in
            self?.displayAsConnectPoints[sender.point!.macAddressString] = true
            self?.pointsCollectionView.reloadData()
        }
        
    }
    
    func disconnectPoint(sender: PointCollectionViewCell){
        let mac = sender.point!.macAddressString
        pxlog("user clicked disconnect on \(mac)", fileName: PointListViewController.fileName)
        DispatchQueue.main.async { [weak self] in
            self?.displayAsConnectPoints[sender.point!.macAddressString] = false
            self?.pointsCollectionView.reloadData()
        }
    }
    
    func showRanges(sender: PointCollectionViewCell){
        

    }
    
    func setCoordinator(sender: PointCollectionViewCell){
        
        let desiredCoordinator = sender.point!
        //pxlog("user clicked setCoordinator: \(desiredCoordinator)", fileName: PointListViewController.fileName)
        currentCoordinator = sender.point?.macAddressString
        
        DispatchQueue.main.async { [weak self] in
            self?.displayAsConnectPoints[desiredCoordinator.macAddressString] = false
            self?.pointsCollectionView.reloadData()
        }
        
    }
    
    func showAlert(title:String, message:String, handler: ((UIAlertAction) -> Swift.Void)? = nil){
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: handler))
        self.present(alertController, animated: true, completion: nil)
    }

    func showErrorAlert(message:String, handler: ((UIAlertAction) -> Swift.Void)? = nil){
        showAlert(title: "Error", message: message, handler: handler)
    }


}
