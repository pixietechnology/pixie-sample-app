import UIKit
import PXPixieSDK_New

class AuthenticationViewController: BaseViewController,UITextFieldDelegate {

    @IBOutlet weak var busyIndicator: UIActivityIndicatorView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    private var currentTextField: UITextField?
    private var email:String = ""
    private var password:String = ""
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AuthenticationViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        
        self.busyIndicator.isHidden = true
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        
        self.emailField.delegate = self
        self.emailField.tag = 1
        
        self.passwordField.delegate = self
        self.passwordField.tag = 2
        
        super.viewDidLoad()
    }
    
    //MARK: -
    //MARK: Keyboard events
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // User finished typing (hit return): hide the keyboard.
        if (textField.tag == 1){
            currentTextField = passwordField
            currentTextField?.becomeFirstResponder()
            return false
        }
        else{
            textField.resignFirstResponder()
            self.onLoginButtonClick(sender: self.loginButton)
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1:
            email = textField.text!
        case 2:
            password = textField.text!
        default: break
        }
    }
    
    //MARK: -
    //Login button
    
    @IBAction func onLoginButtonClick(sender: AnyObject) {
        
        let email = self.emailField.text!
        let password = self.passwordField.text!
        self.busyIndicator.startAnimating()
        self.busyIndicator.isHidden = false
        
        if let currentTextField = currentTextField {
            currentTextField.resignFirstResponder()
        }
        
        sdk.login(userEmail: email, userPassword: password, completion: { [weak self] result, remoteCoordinator, error in
            
              self?.busyIndicator.stopAnimating()
              self?.busyIndicator.isHidden = true
        
              guard error == nil else {
                print(error!.simpleDescription())
                DispatchQueue.main.async {
                  self?.showAlert(title: "Login Error", message: error!.simpleDescription())
                }
                return
              }
          
              if result != PXAccountResult.OK{
                  print(result.simpleDescription())
                  DispatchQueue.main.async {
                      self?.showAlert(title: "Login Error", message: result.simpleDescription())
                  }
                  return
              }
          
              DispatchQueue.main.async {
                  self?.dismiss(animated: false, completion: nil)
              }
          })
    }
}
