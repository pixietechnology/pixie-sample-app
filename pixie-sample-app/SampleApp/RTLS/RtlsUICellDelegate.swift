import UIKit

protocol RtlsUICellDelegate : class{
    func setDesired(sender: PointCollectionViewCell)
    func includeInRTLS(sender: PointCollectionViewCell)
    func excludeFromRTLS(sender: PointCollectionViewCell)
}
