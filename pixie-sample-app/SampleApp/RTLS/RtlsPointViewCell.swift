import UIKit

class RtlsPointViewCell: PointCollectionViewCell {
    
    @IBOutlet weak var lblMacAddress: UILabel!
    @IBOutlet weak var btnCoordinator: UIButton!
    @IBOutlet weak var btnSelect: UIButton!

    var displayAsDesired: Bool = false
    var displayAsIncluded: Bool = false
    
    override func draw() {
        
        guard let _point = self.point else{
            return
        }
        
        self.lblMacAddress.text = _point.macAddressString
        
        self.btnCoordinator.backgroundColor = self.displayAsDesired ? UIColor.blue : UIColor.gray
        self.btnSelect.backgroundColor = self.displayAsIncluded ? UIColor.purple : UIColor.gray
        
        self.btnSelect.setTitle(self.displayAsIncluded ? "Exclude" : "Include", for: .normal)
        self.btnSelect.isHidden = self.displayAsDesired
        let isConnected = _point.pointState == .CONNECTED && _point.canRequestRanges == true
        self.btnSelect.isHidden = !isConnected || displayAsDesired
        self.btnCoordinator.isHidden  = !isConnected || (!displayAsIncluded && !displayAsDesired)
        self.backgroundColor = _point.pointColor.uiColor.withAlphaComponent(isConnected ? 1 : 0.1)
    }
    
    //MARK: -
    // Cell Buttons
    @IBAction func didPressSelect(_ sender: Any) {
        
        if(!self.displayAsIncluded) {
            self.rtlsDelegate?.includeInRTLS(sender: self)
        }
        else {
            self.rtlsDelegate?.excludeFromRTLS(sender: self)
        }
    }
    
    @IBAction func didPressCoordinator(_ sender: Any) {
        self.rtlsDelegate?.setDesired(sender: self)
    }
    
}
