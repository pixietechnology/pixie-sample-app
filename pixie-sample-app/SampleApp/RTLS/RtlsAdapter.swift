import Foundation
import PXPixieSDK_New

class RtlsAdapter: RtlsSessionResponder{
    
    /// Observes map changes
    ///
    /// - Parameter map: live map update
    public func onMap(map: PXPixieSDK_New.Map){
        //HINT 3: on map event send a live map updates
        print("rtls map: \(map.toJson())")
    }
    
    /// Observes ranges while map is updated
    ///
    /// - Parameter ranges: ranges
    public func onRanges(ranges: [PXPixieSDK_New.PXRange]){
        //HINT 4: on range events, you can use range raw data if needed
        
        ranges.forEach({ (range) in
            /*
             ------------
             DEVELOPER ENTRY POINT 1
             
             Here you will actually get the ranges (range.range).
             make sure that the range status is good (range.rangeStatus == .Success)
             ------------
             */
            
            let rangeStatus: String = range.rangeStatus == .Success ? "\(range.range)": "error"
            print("rtls range from \(range.pair.0.macAddressString) to \(range.pair.1.macAddressString): \(rangeStatus)")
        })
    }
}
