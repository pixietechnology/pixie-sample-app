import UIKit
import PXPixieSDK_New

class RtlsViewController: UIViewController, RtlsUICellDelegate, UICollectionViewDataSource, PixiePointDelegate {
    
    @IBOutlet weak var rtlsButton: UIButton!
    @IBOutlet weak var pointsCollectionView: UICollectionView!
    @IBOutlet weak var sessionNumberLabel: UILabel!
    @IBOutlet weak var pinView: UIView!
    static let fileName = "RtlsViewController.swift"

    var displayAsRTLSPoints:[String:Bool] = [:]
    var currentDesired : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sessionNumberLabel.text = "Not Set"
    }

    override func viewDidAppear(_ animated: Bool) {

        self.pointsCollectionView.dataSource = self
        
        self.pointsCollectionView.isHidden = false
        
        self.pointsCollectionView.reloadData()
        
        self.currentDesired = sdk.coordinatorPoint?.macAddressString
        
        super.viewDidAppear(animated)
        
        for point in sdk.pairedPoints {
            self.displayAsRTLSPoints[point.macAddressString] = false
        }
    }

    //MARK: -
    //MARK: PixiePointDelegate
    
    func pixiePointDidConnect(point pixiePoint: PXPixiePoint){
        pxlog("Did connect to \(pixiePoint.macAddressString)", fileName: RtlsViewController.fileName)
        
        DispatchQueue.main.async { [weak self] in
            self?.pointsCollectionView.reloadData()
        }
    }
    
    func pixiePointDidDisconnect(point pixiePoint: PXPixiePoint){
        pxlog("Did disconnect from \(pixiePoint.macAddressString)", fileName: RtlsViewController.fileName)
        
        DispatchQueue.main.async { [weak self] in
            self?.pointsCollectionView.reloadData()
        }
    }
    
    public func point(point: PXPixieSDK_New.PXPixiePoint, didReadBatLevel level: UInt8?, withStatus: PXPixieSDK_New.PXReadBatteryResult){}
    
    public func point(point: PXPixieSDK_New.PXPixiePoint, didUpdateBatteryStatus batteryStatus: PXPixieSDK_New.Pixie.BatteryStatus){}
    
    func didRefreshPoints() { }
    
    //MARK: -
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return sdk.pairedPoints.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RTLSCell", for: indexPath as IndexPath) as! RtlsPointViewCell
        
        let point = sdk.pairedPoints[indexPath.row]
        cell.rtlsDelegate = self
        cell.point = point
        sdk.setPointDelegate(point: point, delegate: self)

        cell.displayAsDesired = self.currentDesired == cell.point?.macAddressString
        
        let isConnect = self.displayAsRTLSPoints.contains(where: { k, v -> Bool in
            return k == point.macAddressString && v == true
        })
        cell.displayAsIncluded = isConnect
        cell.draw()
        
        return cell
    }
    
    var session: RtlsSession?
    var rtlsAdapter: RtlsAdapter?
    
    func startRTLS(){
        
        guard let coordinator = self.currentDesired else {
            showErrorAlert(message: "You must choose a desired")
            rtlsButton.isEnabled = true
            return
        }
        self.displayAsRTLSPoints[coordinator] = true

        let pointsIncluded = self.displayAsRTLSPoints.filter({ (mac,connectable) -> Bool in
            connectable == true
        }).flatMap {$0.key}
        var pointsExcluded = self.displayAsRTLSPoints.filter({ (mac,connectable) -> Bool in
            connectable == false
        }).flatMap {$0.key}
        if let index = pointsExcluded.index(of: coordinator) {
            pointsExcluded.remove(at: index)
        }
        
        guard pointsIncluded.count == 4 else {
            showErrorAlert(message: "You must choose exactly 1 desired and 3 anchors")
            rtlsButton.isEnabled = true
            return
        }
        let desiredPoint = sdk.pairedPoints.filter { $0.macAddressString == self.currentDesired! }.first!
        let anchorPoints = sdk.pairedPoints.filter { (p) -> Bool in
            return p.macAddressString != currentDesired! && pointsIncluded.contains(p.macAddressString)
        }
        self.pinView.isHidden = false
        self.sessionNumberLabel.text = "waiting ..."

        self.rtlsAdapter = RtlsAdapter()
        sdk.createRtlsSession(desiredPoint, anchorPoints[0], anchorPoints[1], anchorPoints[2], self.rtlsAdapter!, { (session, map) in
            self.session = session
            
            pxlogD("init session desired: \(desiredPoint.macAddressString), anchors: \(anchorPoints.flatMap({ $0.macAddressString }))", fileName: RtlsViewController.fileName)
            
            pxlogD("did start session: \(session)", fileName: RtlsViewController.fileName)
            DispatchQueue.main.async {
                
                self.sessionNumberLabel.text = "Running"
                self.rtlsButton.isEnabled = true
                self.rtlsButton.setTitle("Stop", for: UIControlState.normal)
                self.rtlsButton.backgroundColor = UIColor.red
            }
            
            //HINT 1: The developer can decide to move the points and then send the new map at this stage, before calling "start"

            self.session?.start(layoutMap: map , completion: {
                print("rtls session was started")
                //HINT 2: you should start receiving maps updates on RtlsAdapter
            })
        })
    }
    
    func stopRTLS(){
        
        pxlogD("did stop session: \(self.sessionNumberLabel.text!)", fileName: RtlsViewController.fileName)
        self.sessionNumberLabel.text = "ending ..."
        self.session?.stop(completion: { [unowned self] in
            print("rtls session stopped")
            
            self.session = nil
            DispatchQueue.main.async {
                self.rtlsButton.isEnabled = true
                self.pinView.isHidden = true
                self.sessionNumberLabel.text = ""
                self.rtlsButton.setTitle("Start", for: UIControlState.normal)
                self.rtlsButton.backgroundColor = UIColor.gray
            }
        })
    }
    
    //RTLS start
    //make an alert to say now open the RTLS simulator
    @IBAction func didPressConnect(_ sender: Any) {
        
        let rtlsButton = (sender as! UIButton)
        rtlsButton.isEnabled = false

        if rtlsButton.backgroundColor == UIColor.red {
            pxlogD("did press stop rtls: \(self.sessionNumberLabel.text!)", fileName: RtlsViewController.fileName)
            stopRTLS()
            return
        }
        
        pxlogD("did press start rtls ...", fileName: RtlsViewController.fileName)
        startRTLS()
    }

    @IBAction func didPressBAck(_ sender: Any) {
        pxlogD("did press start back", fileName: RtlsViewController.fileName)
        stopRTLS()
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: -
    //MARK: RTLSCellOperations
    
    func includeInRTLS(sender: PointCollectionViewCell) {
        
        let mac = sender.point!.macAddressString
        pxlog("user clicked include on \(mac)", fileName: PointListViewController.fileName)
        DispatchQueue.main.async { [weak self] in
            self?.displayAsRTLSPoints[sender.point!.macAddressString] = true
            self?.pointsCollectionView.reloadData()
        }
    }
    
    func excludeFromRTLS(sender: PointCollectionViewCell) {
        let mac = sender.point!.macAddressString
        pxlog("user clicked exclude on \(mac)", fileName: PointListViewController.fileName)
        DispatchQueue.main.async { [weak self] in
            self?.displayAsRTLSPoints[sender.point!.macAddressString] = false
            self?.pointsCollectionView.reloadData()
        }
    }

    func setDesired(sender: PointCollectionViewCell){
        
        let desiredCoordinator = sender.point!
        pxlog("user clicked setDesired: \(desiredCoordinator)", fileName: PointListViewController.fileName)
        currentDesired = sender.point?.macAddressString
        
        DispatchQueue.main.async { [weak self] in
            self?.displayAsRTLSPoints[desiredCoordinator.macAddressString] = true
            self?.pointsCollectionView.reloadData()
        }
    }
    
    func showAlert(title:String, message:String, handler: ((UIAlertAction) -> Swift.Void)? = nil){
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        pxlogD("alert: \(message)", fileName: RtlsViewController.fileName)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: handler))
        self.present(alertController, animated: true, completion: nil)
    }

    func showErrorAlert(message:String, handler: ((UIAlertAction) -> Swift.Void)? = nil){
        showAlert(title: "Error", message: message, handler: handler)
    }
    
}
