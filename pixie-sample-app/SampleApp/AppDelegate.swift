import UIKit
import PXPixieSDK_New

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{

    var window: UIWindow?
    private var filePath: String!
    
    var pixieSdk:PixieSDKApi!
    
    class AppLogger:IHostLogger{
        func logMessage(message: String){
            NSLog(message)
        }
    }
   
    func connectedPixiesOnOtherApp(note : NSNotification) {
        NSLog("connectedPixiesOnOtherApp detected")
        
        let alert = UIAlertView(title: "Error", message: "Pixies are connected to another app. please kill that app in order to connect to tags", delegate: nil, cancelButtonTitle: "OK")
        alert.show()
    }
    
    public func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool{
        
        HostLogger = AppLogger()
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.connectedPixiesOnOtherApp(note:)), name: connectedPixiesOnOtherAppNotificationName, object: nil)
        
        self.pixieSdk = PixieSDKApi(completion: { (error) in
          guard error == nil else {
            print(error!.simpleDescription())
            return
          }
          print("SDK was initialized successfully")
        })
        
        self.pixieSdk.onUserForcedOutBlock = {
            UIApplication.shared.keyWindow?.rootViewController?.performSegue(withIdentifier: "goToAuthenticate", sender: nil)
        }
        
        return false
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.isIdleTimerDisabled = true
    }
}

public var sdk:PixieSDKApi{
    get{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.pixieSdk
    }
}

