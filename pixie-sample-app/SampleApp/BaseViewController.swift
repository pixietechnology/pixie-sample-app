import Foundation
import UIKit
import PXPixieSDK_New

class BaseViewController:UIViewController{
    
    @IBOutlet weak var lblVersion: UILabel!
    
    func showAlert(title:String, message:String, handler: ((UIAlertAction) -> Swift.Void)? = nil){
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: handler))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showErrorAlert(message:String, handler: ((UIAlertAction) -> Swift.Void)? = nil){
        showAlert(title: "Error", message: message, handler: handler)
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    override func viewDidLoad() {
        
        self.displayVersions()
        
        super.viewDidLoad()
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        
        sdk.saveLogs { [weak self] path in
            guard let weakSelf = self else { return }
            DispatchQueue.main.async {
                weakSelf.showAlert(title: "Logging", message: "New log saved: \(path ?? "")")
            }
        }
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        get{
            return .portrait
        }
    }
    
    //MARK: -
    //MARK: Version Info
    private func displayVersions(){
        var version = ""
        if let v = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String{
            let b = Bundle.main.infoDictionary![kCFBundleVersionKey as String] as! String
            version = "\(v) (\(b))"
        }
        self.lblVersion.text = "App version: " + version + " | SDK version: " + PXVersionManager.sharedInstance.getSDKVersion()
    }
}

