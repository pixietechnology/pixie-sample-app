//
//  ValueWithStatus.h
//  PixieAlgo
//
//  Created by Raja Tabar on 20/03/2017.
//  Copyright © 2017 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValueWithStatus : NSObject

@property double value;
@property bool status;
@property bool isUnitLess;

-(instancetype)initWithValue:(double)value andWithStatus:(bool)status andWithUnitLess:(bool)isUnitLess;

@end
