//
//  PXLoggingBridge.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 9/16/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//
#ifndef __PXLOGGINGBRIDGE_H__
#define __PXLOGGINGBRIDGE_H__

#import <Foundation/Foundation.h>

@class PXInMemoryLogger;

@interface PXLoggingBridge : NSObject

@property (atomic) PXInMemoryLogger *inMemoryLoggerInstance;

+ (id)sharedInstance;

-(void)buffer:(char*)log toFile:(const char*)fileName;

@end

#endif