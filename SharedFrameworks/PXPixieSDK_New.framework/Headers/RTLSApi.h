//
//  RTLSApi.h
//  PixieAlgo
//
//  Created by Raja Tabar on 08/03/2017.
//  Copyright © 2017 Pixie. All rights reserved.
//

#ifndef PXRTLSApi_h
#define PXRTLSApi_h

#import "RTLSMap.h"

@protocol RTLSApiDelegate <NSObject>
-(void) onMap:(RTLSMap*) _map;
@end

@interface RTLSApi : NSObject

@property (assign, nonatomic) id<RTLSApiDelegate> delegate;
@property (readonly) void * cppBridge;

-(void) onMap:(RTLSMap*) _map;
@end

#endif /* PXRTLSApi_h */
