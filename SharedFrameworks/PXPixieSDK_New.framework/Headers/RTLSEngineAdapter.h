//
//  RTLSEngineAdapter.h
//  PXSWBleManagerTestApp
//
//  Created by Saggi Messer on 8/27/14.
//  Copyright (c) 2014 Pixie. All rights reserved.
//

#ifndef __PXLOCATIONENGINEADAPTER_H__
#define __PXLOCATIONENGINEADAPTER_H__

#import <Foundation/Foundation.h>
#import "PXLoggingBridgeHeader.h"
#import "RTLSApi.h"
#import "RangePair.h"
#import "RangeTimeInfo.h"






@interface RTLSEngineAdapter : NSObject


@property (weak,nonatomic) id<RTLSApiDelegate> delegate;


-(instancetype)initRTLSWith:(RtlsPoint*)desired withMaxSpeed:(double)maxSpeed anchor1:(RtlsPoint*)anchor1 anchor2:(RtlsPoint*)anchor2 anchor3:(RtlsPoint*)anchor3 andDelegate:(id<RTLSApiDelegate>)delegate;


// data input methods

-(void)inputRange:(NSArray<RangePair*>*)ranging andWithTimeInfo:(RangeTimeInfo*)timeInfo;

@end

#endif
