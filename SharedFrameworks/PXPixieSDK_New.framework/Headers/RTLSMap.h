//
//  RTLSMap.h
//  PixieAlgo
//
//  Created by Raja Tabar on 20/03/2017.
//  Copyright © 2017 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RtlsPoint.h"

enum ResponseCode {
    success = 0,                                 // No warning & no errors
    objectHeightDefinedByCode   = 0x00001,       // Warning: Object height defined by code
    anchorsHeightDefinedByCode  = 0x00002,       // Warning: Anchors height defined by code
    anchorsXYdefinedByCode      = 0x00004,       // Warning: Anchors XY coordinates defined by code and coordinate units set to cm.
    targetSpeedDefinedByCode    = 0x00008,       // Warning: Target speed limited or defined by code
    speedDefinedAsHighSpeed     = 0x00010,       // Warning: Speed defined as high speed (no filter) with a unique speed code (4614)
    locationInsufficientRanges  = 0x00100,       // Warning: Tag location was not updated due to insufficient ranges
    locationContradictingRanges = 0x00200,       // Warning: Tag location was not updated due to contradicting measured ranges (too short)
    noAnchorXYrangeError        = 0x00400,       // Error: XY coordinates of anchors could not be resolved due to range errors (no triangle)
    noAnchorXYnoRanges          = 0x00800,       // Error: XY coordinates of anchors could not be resolved since ranges were not available
    xyAnchorsAlmostLinear       = 0x01000,       // Error: XY coordinates of anchors form an unstable solution (almost linear)
    scalingFailedMismatch       = 0x02000,       // Error: Scaling of input coordinates failed due to mismatch in proportions
    scalingFailedNoRanges       = 0x04000,       // Error: Scaling of input coordinates failed since ranges were not available.
    errorAnchorUnits            = 0x08000,       // Error: Inconsistency in anchor units (mismatch between pixels and cm)
    errorAnchorXYvalidity       = 0x10000        // Error: Inconsistency in anchor XY validity (some anchors are valid and some are not)
};

@interface RTLSMap : NSObject

//@property NSArray<RtlsPoint*> *points;
@property RtlsPoint* desired;
@property RtlsPoint* anchor1;
@property RtlsPoint* anchor2;
@property RtlsPoint* anchor3;
@property double inputCoordinatesScaling;

@property enum ResponseCode status;

- (instancetype)initWithDesired:(RtlsPoint*) desired
                 andWithAnchor1:(RtlsPoint*) anchor1 andWithAnchor2:(RtlsPoint*) anchor2 andWithAnchor3:(RtlsPoint*) anchor3
                  andWithStatus:(enum ResponseCode)status andWithScale:(double) scale;

@end

//struct RTLSMap
//{
//    RTLSMap(RtlsPoint _desired, RtlsPoint _anchor1, RtlsPoint _anchor2, RtlsPoint _anchor3);
//    
//    RtlsPoint   desired;
//    RtlsPoint   anchor1;
//    RtlsPoint   anchor2;
//    RtlsPoint   anchor3;
//    
//    enum ResponseCode status;
//};
