//
//  RangePair.h
//  PixieAlgo
//
//  Created by Raja Tabar on 20/03/2017.
//  Copyright © 2017 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ValueWithStatus.h"

@interface RangePair : NSObject

@property UInt64 fromMacAddress;
@property UInt64 toMacAddress;
@property ValueWithStatus* range;

- (instancetype)initWithFromId:(UInt64)fromMacAddress andWithToId:(UInt64)toMacAddress andWithRange:(ValueWithStatus*)range;

@end
