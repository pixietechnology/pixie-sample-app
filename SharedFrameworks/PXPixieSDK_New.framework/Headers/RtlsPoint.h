//
//  RtlsPoint.h
//  PixieAlgo
//
//  Created by Raja Tabar on 20/03/2017.
//  Copyright © 2017 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ValueWithStatus.h"

@interface RtlsPoint : NSObject

@property UInt64 macAddress;
@property ValueWithStatus* x;
@property ValueWithStatus* y;
@property ValueWithStatus* z;

- (instancetype)initWithId:(UInt64)macAddress andWithX:(ValueWithStatus*)x andWithY:(ValueWithStatus*)y andWithZ:(ValueWithStatus*)z;

@end
