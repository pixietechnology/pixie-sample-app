//
//  RangeTimeInfo.h
//  PixieRtls
//
//  Created by Raja Tabar on 26/03/2017.
//  Copyright © 2017 Pixie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RangeTimeInfo : NSObject

@property double sendTime;          // t1
@property double commandRxTime;     // t2
@property double responseTxTime;    // t3
@property double receiveTime;       // t4
@property double lastRangingTime;   // rangingTimeStamp

- (instancetype)initWithSendTime:(double)sendTime andWithRxTime:(double)commandRxTime andWithTxTime:(double)responseTxTime andWithReceiveTime:(double)receiveTime andWithLastRangingTime:(double)lastRangingTime;

@end
