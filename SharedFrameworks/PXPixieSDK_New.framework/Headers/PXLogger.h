#ifndef __PXLOGGER_H__
#define __PXLOGGER_H__


#import <Foundation/Foundation.h>

@interface PXLogger : NSObject

@property BOOL loggerEnabled, enableNSLog, enableXCodeLog;
@property NSString* folderPath;
@property NSString* allFoldersPath;
@property NSString* fileName;

+ (id) sharedLogger;

-(void) log:(NSString*)log;

-(void) logD:(NSString*)log;

-(void) logE:(NSString*)log;

-(void) flush:(void(^)(NSString* path))handler;

-(void) setPath:(NSString*) folder;

@end

#endif