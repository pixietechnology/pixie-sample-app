//
//  PXInMemoryLogger.h
//  PixieAlgo
//
//  Created by IlanL on 08/06/2016.
//  Copyright © 2016 Pixie. All rights reserved.
//

#ifndef __PXINMEMORYLOGGER_H__
#define __PXINMEMORYLOGGER_H__

#import <Foundation/Foundation.h>

@interface PXInMemoryLogger : NSObject

+ (id)sharedLogger;

@property BOOL loggerEnabled, enableNSLog, enableXCodeLog;
@property NSString* folderPath;
@property NSString* allFoldersPath;

-(void) setPath:(NSString*) folder;

-(void) push:(NSString*)log andFile:(NSString*)filename;

-(void) flush:(void(^)(NSString* path))handler;

@end

#endif